import sbt.Keys._

name := "streaming-kafka"

version := "1.6"

scalaVersion := "2.10.6"

libraryDependencies ++= {
  val sparkVersion =  "1.6.0"
  Seq(

      "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
      "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
      "org.apache.spark" %% "spark-streaming" % sparkVersion % "provided",
      "org.apache.spark" %% "spark-repl" % sparkVersion % "provided",

      "org.apache.kafka" % "kafka-log4j-appender" % "0.10.0.1",
      "org.apache.kafka" % "kafka-clients" % "0.10.0.1",
      "org.apache.kafka" %% "kafka" % "0.10.0.1"
  )
}

unmanagedJars in Compile := (baseDirectory.value ** "*.jar").classpath
unmanagedResourceDirectories in Compile += { baseDirectory.value / "com/ibm/cds/spark/samples/config" }

